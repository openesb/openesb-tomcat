/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIServlet.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.tomcat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JBIServlet handles the lifecycle of JBI inside WebSphere
 *
 * @author Sun Microsystems, Inc.
 */
public class JBIServlet extends HttpServlet
{
   /**
    * an instance of TomcatJBIBootstrap which only holds the
    * JBI system jars jbi.jar, jbi-ext.jar
    */
   private static TomcatJBIBootstrap jbiBootstrap;

    /**
     * Logger used by this class.
     */
    private Logger mLog =
        Logger.getLogger(this.getClass().getPackage().getName());
   
   /**
    * Constructor
    */
   public JBIServlet()
   {
   }

   /**
    * destroy method stops the JBI framework
    */
   public void destroy()
   {
       super.destroy();
       try
       {
           jbiBootstrap.stop(getJBIProperties());
       }
       catch(Exception ex)
       {
           mLog.log(Level.SEVERE, ex.getMessage(), ex);
       }
   }

   /**
     * init method is used to init JBIFramework
     * @throws javax.servlet.ServletException a ServletException is thrown if JBIFramework did not init successfully
     */
   public void init()
       throws ServletException
   {
       super.init();
       initFramework();
   }

   /**
     * the processRequest method that handles both get and post requests
     * the response is a simple page that confirms that JBIServlet is running
     * @param request the request
     * @param response the response
     * @throws ServletException ServletException is thrown
     * @throws java.io.IOException IOException is thrown
     */
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException
   {
       response.setContentType("text/html;charset=UTF-8");
       PrintWriter out = response.getWriter();
       out.println("<html>");
       out.println("<head>");
       out.println("<title>Servlet JBIServlet</title>");
       out.println("</head>");
       out.println("<body>");
       out.println((new StringBuilder()).append("<h1>Servlet JBIServlet at ").append(request.getContextPath()).append("</h1>").toString());
       out.println("</body>");
       out.println("</html>");
       out.close();
   }

   /**
    * This method is used to get required properties from the servlet
    * context and pass it to the framework
    * @return Properties the properties from servlet context
    */ 
   private Properties getJBIProperties()
   {
       Properties props = new Properties();
       Enumeration propNames = getInitParameterNames();

       //props.put("install.root", getServletConfig().getServletContext().getRealPath("/")+"WEB-INF/lib/");
       props.put("install.root",System.getProperty("jbi.install.root"));
       String name;
       for(; propNames.hasMoreElements(); props.put(name, getInitParameter(name)))
           name = (String)propNames.nextElement();

       return props;
   }

   /**
    * This method initializes the JBI Bootstrap
    */
   private synchronized void initFramework()
   {
       try
       {
           if(jbiBootstrap == null)
               jbiBootstrap = new TomcatJBIBootstrap(getJBIProperties());
           if(!jbiBootstrap.isStarted())
               jbiBootstrap.start(getJBIProperties());
       }
       catch(Exception ex)
       {
           mLog.log(Level.SEVERE, ex.getMessage(), ex);
       }
   }

   
   /**
     * doGet method handles the get requests
     * the response is a simple page that confirms that JBIServlet is running
     * @param request the request
     * @param response the response
     * @throws ServletException ServletException is thrown
     * @throws java.io.IOException IOException is thrown
     */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException
   {
       processRequest(request, response);
   }

    /**
     * doPost method handles the post requests
     * the response is a simple page that confirms that JBIServlet is running
     * @param request the request
     * @param response the response
     * @throws ServletException ServletException is thrown
     * @throws java.io.IOException IOException is thrown
     */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException
   {
       processRequest(request, response);
   }

   /**
    * This method returns a short description 
    * @return String the description
    */
   public String getServletInfo()
   {
       return "Short description";
   }


} 
