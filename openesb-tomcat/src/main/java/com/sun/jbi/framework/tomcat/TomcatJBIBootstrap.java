/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TomcatJBIBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.tomcat;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXServiceURL;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.logging.Logger;

/**
 * Provides support for running JBI in Tomcat
 *
 * @author Sun Microsystems, Inc.
 */
public class TomcatJBIBootstrap

{    
    /** JSR208 interfaces. */    
    private static final String JBI_JAR_NAME = "jbi.jar";
    
    /** JBI runtime interfaces exposed to components. */
    private static final String JBI_EXT_JAR_NAME = "jbi-ext.jar";
    
    /** Name of the top-level class of the JBI runtime framework. */
    private static final String JBI_FRAMEWORK_CLASS_NAME =
        "com.sun.jbi.framework.tomcat.TomcatJBIFramework";

    /** Environment property used to override install root location. */
    private static final String INSTALL_ROOT = "install.root";

    /** Environment property used for instance name. */
    public static final String INSTANCE_NAME = "instance.name";
 
    /** Default instance name. */
    private static final String DEFAULT_INSTANCE_NAME = "server";
    
    
    /** List of jars that should not be included in the runtime classloader. */
    private List<String> mBlacklistJars = new ArrayList<String>();
    
    /** ClassLoader used for JBI runtime classes.  These classes are not 
     *  part of the component classloader hierarchy.
     */
    public static ClassLoader mFrameworkClassLoader;
    
    /** JBI installation directory. */
    private File mJbiInstallDir;
    
    /** JBI Framework implementation */
    private Object mJbiFramework;
    
    /** Environment properties */
    private Properties mEnvironment;
    
    /** logger for this bootstrap class */
    private static Logger logger;
  
    /** flag to track if the bootstrap has been called before */
    public static boolean mStarted = false;
    
    /** this resource bundle loads messages used in log*/
    private static ResourceBundle resourceBundle;
     
    
    /**
     * Loads the JBI Bootstrap in WebSphere.
     * @param properties the properties from the servlet context
     */
    public static void start(Properties properties)
    {
        TomcatJBIBootstrap jbiBootstrap = new TomcatJBIBootstrap(properties);
        try
        {
            initResourceBundle();            
            jbiBootstrap.createJBIFramework();
            jbiBootstrap.loadJBIFramework();
            logger.info(getString(LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_LOAD_SUCCESS));
            mStarted = true;            
        }
        catch (Exception ex)
        {
            logger.severe(ex.getMessage());
        }
    }
    
    /**
     * This method is used to find out if JBIBoostrap has been started
     * @return boolean true if JBIBoostrap is started
     */
    public  static boolean isStarted()
    {
        return mStarted;
    }
    
    /**
     * This method is used to stop the JBIBootstrap
     * @param properties tht properties from servlet context
     */
    public static void stop(Properties properties)
    {
        TomcatJBIBootstrap jbiBootstrap = new TomcatJBIBootstrap(properties);
        
        try
        {
            jbiBootstrap.createJBIFramework();
            jbiBootstrap.unloadJBIFramework();
        }
        catch (Exception ex)
        {
            logger.severe(ex.getMessage());
        }
    }    
    
    /** 
     * Create a new TomcatJBIBootstrap instance with the specified environment.
     * @param env environment properties
     */
    public TomcatJBIBootstrap(Properties env)
    {
        mEnvironment = env;
        
        // setup blacklist jars
        //mBlacklistJars.add(JBI_JAR_NAME);
        //mBlacklistJars.add(JBI_EXT_JAR_NAME);
        
        // If install root is not set, default to current working directory
        String installPath = mEnvironment.getProperty(INSTALL_ROOT);
        if (installPath == null)
        {
            File installDir = new File(System.getProperty("user.dir"));
            // account for javaw launch from a double-click on the jar
            if (installDir.getName().equals("lib"))
            {
                installDir = installDir.getParentFile();
            }
            
            installPath = installDir.getAbsolutePath();
        }

        mJbiInstallDir = new File(installPath);
        
        // quick sanity check on the install root
/*        if (!mJbiInstallDir.isDirectory() ||
            !new File(mJbiInstallDir, "jbi_rt.jar").exists())
        {
            throw new RuntimeException(
                    getString(LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_INVALID_INSTALL_ROOT) + 
                    mJbiInstallDir.getAbsolutePath());
        }
  */
        // pass this information along to the core framework
        mEnvironment.setProperty(INSTALL_ROOT, mJbiInstallDir.getAbsolutePath());
        
        logger = Logger.getLogger(this.getClass().getPackage().getName());        
    }
    
    /** 
     *  Loads the JBI framework using the Java SE platform wrapper.  If the
     *  framework loads successfully, this method adds a shutdown hook to 
     *  allow for civilized clean-up when the VM terminates.
     */
    void loadJBIFramework()
    {
        try
        {
            invoke(mJbiFramework, "load");
        }
        catch (Throwable t)
        {
            logger.severe(getString(LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_LOAD_FAILED));
            logger.severe(t.toString());
        }
    }
    
    /** 
     *  Unloads the JBI framework using the Java SE platform wrapper.  This is
     *  always a remote call, since the framework has (presumably) been loaded 
     *  previously by another process.
     */
    void unloadJBIFramework()
    {
        String          errMsg = null;
        JMXServiceURL   serviceURL;
        int             jmxConnectorPort;
        
        try
        {
            MBeanServerConnection mbsConn = (MBeanServerConnection)
                invoke(mJbiFramework, "getMBeanServerConnection");
            ObjectName fwMBeanName = new ObjectName("com.sun.jbi", "instance", 
                    mEnvironment.getProperty(INSTANCE_NAME, DEFAULT_INSTANCE_NAME));
            mbsConn.invoke(fwMBeanName, "unload", new Object[0], new String[0]);
            mbsConn.unregisterMBean(fwMBeanName);
            
        }    
        catch (javax.management.MBeanException mbEx)
        {
            errMsg = mbEx.getTargetException().toString();
        }
        catch (Throwable t)
        {
            errMsg = t.toString();
        }
        
        if (errMsg != null)
        {            
            logger.severe(getString(LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_UNLOAD_FAILED));
            logger.severe(errMsg);
        }
        else
        {
            logger.info(getString(LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_UNLOAD_SUCCESS));
        }
    }
    
    /** 
     * Creates the JBI framework using the appropriate classloading structure.
     */
    private void createJBIFramework()
        throws Exception
    {
        Class       fwClass;
        Constructor fwCtor;
        
        try
        {
            createFrameworkClassLoader();
            fwClass = mFrameworkClassLoader.loadClass(JBI_FRAMEWORK_CLASS_NAME);
            fwCtor = fwClass.getDeclaredConstructor(Properties.class);
            mJbiFramework = fwCtor.newInstance(mEnvironment);


        }
        catch (Exception ex)
        {
            logger.severe(ex.getMessage());            
            throw new Exception(getString(
                    LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_CREATE_FAILED));

        }
    }
    
    /** 
     *  Creates a separate runtime classloader to avoid namespace pollution
     *  between the component classloading hierarchy and the JBI implementation.
     *  At present, this method is greedy and includes any file in the lib/ 
     *  directory in the runtime classpath.
     */
    private void createFrameworkClassLoader()
    {
        ArrayList<URL> cpList = new ArrayList<URL>();
        URL[] cpURLs = new URL[0];
        File libDir = new File(mJbiInstallDir, "lib");
        
        // Everything in the lib directory goes into the classpath
        for (File lib : libDir.listFiles())
        {
            try
            {
                if (mBlacklistJars.contains(lib.getName()))
                {
                    // skip blacklisted jars
                    continue;
                }
                
                cpList.add(lib.toURL());
            }
            catch (java.net.MalformedURLException urlEx)
            {
                logger.warning(
                        getString(LocalStringKeys.WEBSPHERE_JBI_FRAMEWORK_BAD_URL_IN_FRAMEWORK_CLASSPATH));
                logger.warning(urlEx.getMessage());
            }
        }
        
        cpURLs = cpList.toArray(cpURLs);
        mFrameworkClassLoader = new URLClassLoader(
                cpURLs, getClass().getClassLoader());
    }
    
    /**
     * This method is used to invoke a given method in a given object using 
     * reflection. This bootstrap class uses this method to invoke the load
     * method on the framework.
     * @param obj the object in which the method is to be invoked
     * @param method the method name
     * @param params the list of parameteres needed by the method to be invoked
     */
    private Object invoke(Object obj, String method, Object... params)
        throws Throwable
    {
        Object result = null;
        
        try
        {
            for (Method m : obj.getClass().getDeclaredMethods())
            {
                if (m.getName().equals(method))
                {                    
                    result = m.invoke(obj, params);
                    break;
                }
            }
            
            return result;
        }
        catch (java.lang.reflect.InvocationTargetException itEx)
        {
            throw itEx.getTargetException();
        }
    }    
    
    /**
     * This method is used to initialize the resource bundle that is used
     * to load messages used in log
     */
    private static void initResourceBundle()
    {
        try 
        {
           resourceBundle = ResourceBundle.getBundle("com.sun.jbi.framework.websphere.LocalStrings");
        }
        catch (MissingResourceException missingException)
        {
            logger.warning("Resource bundle could not be loaded " + missingException.getMessage());
        }
    }
    
    /**
     * This method is used to get a value of the given key from a resource bundle
     * If the resource bundle is not available this method returns the key itself
     * @param key the key
     * @returns String the value in the bundle for the given key
     */
    private static String getString(String key)
    {
        if (resourceBundle != null)
        {
            return resourceBundle.getString(key);
        }
        else 
        {
            return key;
        }
    }
}
