/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.tomcat;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{ 
    /**
     * Failed to load JBI Framework
     */
    String WEBSPHERE_JBI_FRAMEWORK_LOAD_FAILED =
          "WEBSPHERE_JBI_FRAMEWORK_LOAD_FAILED";
    
    /**
     * Failed to unload JBI Framework
     */
    String WEBSPHERE_JBI_FRAMEWORK_UNLOAD_FAILED =
          "WEBSPHERE_JBI_FRAMEWORK_UNLOAD_FAILED";
    
    /**
     * Failed to unload JBI Framework
     */
    String WEBSPHERE_JBI_FRAMEWORK_UNLOAD_SUCCESS =
          "WEBSPHERE_JBI_FRAMEWORK_UNLOAD_SUCCESS";
    
    /**
     * Failed to create JBI Framework
     */
    String WEBSPHERE_JBI_FRAMEWORK_CREATE_FAILED =
            "WEBSPHERE_JBI_FRAMEWORK_CREATE_FAILED";
    
    /**
     * Bad URL in framework classpath
     */
    String WEBSPHERE_JBI_FRAMEWORK_BAD_URL_IN_FRAMEWORK_CLASSPATH =
            "WEBSPHERE_JBI_FRAMEWORK_BAD_URL_IN_FRAMEWORK_CLASSPATH";

    /**
     * Invalid install root
     */
    String WEBSPHERE_JBI_FRAMEWORK_INVALID_INSTALL_ROOT =
            "WEBSPHERE_JBI_FRAMEWORK_INVALID_INSTALL_ROOT";    
    
    /**
     * JBI framework load success
     */
    String WEBSPHERE_JBI_FRAMEWORK_LOAD_SUCCESS =
            "WEBSPHERE_JBI_FRAMEWORK_LOAD_SUCCESS";
    
}
